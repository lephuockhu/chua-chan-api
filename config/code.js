module.exports = {
  CODE_SUCCESS: 200, //successful
  CODE_UNAUTHORIZED: 401, //unauthorized
  CODE_ERROR: 500, //error
  CODE_MISS_PARAMETER: 402, //miss parameter
  CODE_BAD_REQUEST: 400, //bad request
  CODE_UNSUPPORTED_API: 501, //unsupported api
}