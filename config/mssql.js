const { mssql } = require('config');

const { user, password, server, database } = mssql

const connection = {
  user,
  password,
  server,
  database,
  option: {
    encrypt: true
  }
}

module.exports = {
  connection
};
