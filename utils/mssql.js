const _ = require('lodash');
const sql = require('mssql');
const log4js = require('log4js');

const logger = log4js.getLogger('Mssql');
const loggerError = log4js.getLogger('MssqlError');

const { connection } = require(`${process.cwd()}/config/mssql.js`);

logger.setLevel('ALL');
loggerError.setLevel('ERROR');


function query(strQuery) {
  return new Promise(async (resolve, reject) => {
    let startTime = Date.now();
    try {
      const pool = new sql.ConnectionPool(connection);
      await pool.connect();
      let request = pool.request();
      let result = await request.query(strQuery);

      let endTime = Date.now();
      let duration = endTime - startTime;

      logger.info("executed query: ", { strQuery, duration });

      pool.close();

      return resolve(result.recordsets);
    } catch (err) {
      loggerError.error("cannot execute query: ", strQuery);
      loggerError.error("error:", err);

      pool.close();
      return reject(err);
    }
  }
  )
};

// getAssociatedObjects: "get_associated_objects :NUMBER(userId), :NUMBER(portalId), :NUMBER(ticketId), :CURSOR"
function getFieldsFromStr(strQuery) {
  const regexMacro = /(\([^\)]+\))/g;

  let fields = strQuery.match(regexMacro);

  fields = _.map(fields, (field) => {
    return field.replace(/\(/g, "").replace(/\)/g, "")
  })

  return fields
}

function convertInputToQueryDB(strQuery, inputs) {
  let arrQuery = strQuery.split(' ');
  let spName = arrQuery[0];

  let fields = getFieldsFromStr(strQuery);

  logger.debug("spName: ", spName);
  logger.debug("inputs: ", fields.join(','));

  let values = []

  fields.forEach(field => {
    let value = inputs[field];

    logger.debug(`fieldName: ${field}, value: ${value}`);

    if (value) {
      values.push(value);
    }
  });

  logger.debug("values: ", values);

  let strvalues = values.join(', ')
  // let strExecuteQuery = values.length > 0 ? `EXEC ${spName} ${values.join(', ')}` : `EXEC ${spName}`
  let strExecuteQuery = `EXEC ${spName} ${strvalues}`;

  let entries = query(strExecuteQuery);

  return entries;
}

function convertDataTypeToTypeSQL(dataType) {
  let type = sql.NVarChar;

  switch (dataType) {
    case 'string':
      type = sql.NVarChar;
      break;
    case 'number':
      type = sql.Int;
      break;
    case 'boolean ':
      type = sql.Bit;
      break;
    case 'date ':
      type = sql.DateTime;
      break;
    case 'buffer ':
      type = sql.VarBinary;
      break;
    case 'table':
      type = sql.TVP;
      break;

    default:
      type = sql.NVarChar;
      break;
  }

  return type;
}

function executeSP(procedure, inputs, output) {
  return new Promise(async (resolve, reject) => {
    let startTime = Date.now();
    try {
      const pool = new sql.ConnectionPool(connection);
      const request = new sql.Request();

      logger.debug("---input: ");
      inputs.forEach(({ parameter, dataType, value }) => {
        logger.debug("parameter: ", parameter);
        logger.debug("dataType: ", dataType);
        logger.debug("value: ", value);

        const type = convertDataTypeToTypeSQL(dataType);
        logger.debug("type: ", type);

        request.input(parameter, type, value);
      });

      if (!output) {
        logger.debug("---output: ");
        const { parameter, dataType, value } = output;
        logger.debug("parameter: ", parameter);
        logger.debug("dataType: ", dataType);
        logger.debug("value: ", value);

        const type = convertDataTypeToTypeSQL(dataType);
        logger.debug("type: ", type);

        request.output(parameter, type, value);
      }

      let result = await request.execute(procedure);

      let endTime = Date.now();
      let duration = endTime - startTime;

      logger.debug("executed call SP: ", { procedure, duration });

      pool.close();

      return resolve(result);
    } catch (error) {
      loggerError.error("cannot execute call SP: ", procedure);
      loggerError.error("error:", error);

      pool.close();
      return reject(error);
    }
  })
}

module.exports = {
  query,
  executeSP,
  convertInputToQueryDB
}