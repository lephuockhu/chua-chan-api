const moment = require('moment');

function getDateNowByFormat(strFormat = 'YYYY-MM-DD') {
  return moment().format(strFormat);
}

module.exports = {
  getDateNowByFormat,
}