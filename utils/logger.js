const _ = require('lodash');
const log4js = require('log4js');
const config = require('config');

log4js.configure(`${process.cwd()}/config/log4js.json`);

class Logger {
  constructor(name, level) {
    var logger;
    logger = log4js.getLogger(name || 'app');
    logger.setLevel(config['log'][level]);
    _.extend(this, logger);
  }

};

module.exports = Logger;
