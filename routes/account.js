const express = require('express');

const router = express.Router();

const Controllers = require('../controllers');

const AccountController = Controllers['AccountController'];

router.post('/', AccountController.login);

module.exports = router;