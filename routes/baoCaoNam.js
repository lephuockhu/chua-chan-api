const express = require('express');

const router = express.Router();

const Controllers = require('../controllers');

const baoCaoNamController = Controllers['baoCaoNamController'];

router.get('/', baoCaoNamController.getAll);

module.exports = router;