const express = require('express');

const router = express.Router();

const Controllers = require('../controllers');

const NgayNhanVienController = Controllers['NgayNhanVienController'];

router.get('/', NgayNhanVienController.getAll);

module.exports = router;