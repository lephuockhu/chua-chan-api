const express = require('express');

const router = express.Router();

const Controllers = require('../controllers');

const NgayKhachDoanController = Controllers['NgayKhachDoanController'];

router.get('/', NgayKhachDoanController.getAll);

module.exports = router;