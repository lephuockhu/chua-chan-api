const express = require('express');

const router = express.Router();

const Controllers = require('../controllers');

const NhanVienController = Controllers['NhanVienController'];

router.get('/', NhanVienController.getAll);

module.exports = router;