const express = require('express');

const router = express.Router();

const Controllers = require('../controllers');

const baoCaoTuanThangController = Controllers['baoCaoTuanThangController'];

router.get('/', baoCaoTuanThangController.getAll);

module.exports = router;