const jwt = require('jsonwebtoken');
const config = require('config');

const { convertInputToQueryDB } = require(`${process.cwd()}/utils/mssql.js`);
const packages = require('../mssql/package');
const code = require('../config/code');

function generateToken(user) {
  console.log(JSON.stringify({ maNhanVien: user.MaNhanVien, maTaiKhoan: user.MaTaiKhoan }))
  token = jwt.sign({ maNhanVien: user.MaNhanVien, maTaiKhoan: user.MaTaiKhoan }, config['jwtSecretKey'], { expiresIn: 30 * 24 * 60 * 60 });

  return token;
}

class AccountController {
  constructor(logger) {
    this.logger = logger;
  }


  async login(req, res, next) {
    console.log("AccountController:login is called");
    let { taiKhoan, matKhau } = req.body
    try {
      let result = {
        code: code.CODE_SUCCESS,
        message: 'Success',
        data: {},
      }

      let inputs = {
        taiKhoan: `'${taiKhoan}'`,
        matKhau: `'${matKhau}'`,
      }
      console.log("inputs: ", inputs);

      let infoAccount = await convertInputToQueryDB(packages.loginAccount, inputs);
      infoAccount = infoAccount[0];

      if (infoAccount.length === 0) {
        return res.json({
          code: code.CODE_UNAUTHORIZED,
          message: 'Account login fail',
          data: {}
        })
      }



      let infoAccountResult = {
        maNhanVien: infoAccount[0].MaNhanVien,
        tenNhanVien: infoAccount[0].TenNhanVien,
        maTaiKhoan: infoAccount[0].MaTaiKhoan,
        token: generateToken(infoAccount[0]),
      }

      result.data = {
        entries: infoAccountResult,
        meta: {
          total: 1,
        }
      }

      return res.json(result);
    } catch (error) {
      console.error("AccountController::login::error: ", error);

      return next(error);
    }
  }
}

module.exports = AccountController;