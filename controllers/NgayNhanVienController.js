const _ = require('lodash');

const { convertInputToQueryDB } = require(`${process.cwd()}/utils/mssql.js`);
const packages = require('../mssql/package');
const code = require('../config/code');
const helper = require('../utils/helper');

class NgayNhanVienController {
  constructor(logger) {
    this.logger = logger;
  }

  async getAll(req, res, next) {
    console.log("NgayNhanVienController:getAll is called");
    let { ngayNap, viTri, maGroupLoaiVe, maNhanVien } = req.query
    try {
      let result = {
        code: code.CODE_SUCCESS,
        message: 'Success',
        data: {},
      }

      ngayNap = ngayNap || helper.getDateNowByFormat('YYYY-MM-DD');

      viTri = +viTri || 1

      maGroupLoaiVe = +maGroupLoaiVe || 2

      maNhanVien = +maNhanVien || 63

      let inputs = {
        ngayNap: `'${ngayNap}'`,
        viTri,
        maGroupLoaiVe,
        maNhanVien,
      }
      console.log("inputs: ", inputs);

      let doanhThuNhanVien = await convertInputToQueryDB(packages.doanhThuNgayNhanVienBanVe, inputs);
      doanhThuNhanVien = _.first(doanhThuNhanVien);

      result.data = {
        entries: doanhThuNhanVien,
        meta: {
          total: doanhThuNhanVien.length,
        }
      }

      return res.json(result);
    } catch (error) {
      console.error("NgayNhanVienController::getAll::error: ", error);

      return next(error);
    }
  }
}

module.exports = NgayNhanVienController;