const _ = require('lodash');

const { convertInputToQueryDB } = require(`${process.cwd()}/utils/mssql.js`);
const packages = require('../mssql/package');
const code = require('../config/code');
const helper = require('../utils/helper');

class baoCaoNamController {
  constructor(logger) {
    this.logger = logger;
  }

  async getAll(req, res, next) {
    console.log("baoCaoNamController:getAll is called");
    let { nam, maGroupLoaiVe } = req.query
    try {
      let result = {
        code: code.CODE_SUCCESS,
        message: 'Success',
        data: {},
      }

      nam = nam || helper.getDateNowByFormat('YYYY');

      maGroupLoaiVe = +maGroupLoaiVe || 1

      let inputs = {
        maGroupLoaiVe,
        nam,
      }
      console.log("inputs: ", inputs);

      let baoCaoNam = await convertInputToQueryDB(packages.baoCaoDoanhThuNam, inputs);
      baoCaoNam = _.first(baoCaoNam);

      result.data = {
        entries: baoCaoNam,
        meta: {
          total: baoCaoNam.length,
        }
      }

      return res.json(result);
    } catch (error) {
      console.error("baoCaoNamController::getAll::error: ", error);

      return next(error);
    }
  }
}

module.exports = baoCaoNamController;