const requireAll = require('require-all');

const log4js = require('log4js');

//config logger
log4js.configure(`${process.cwd()}/config/log4js.json`);

const logger = log4js.getLogger('Controllers');

logger.setLevel('ALL');

module.exports = requireAll({
  dirname: __dirname,
  filter: /(.+Controller)\.js$/,
  resolve: function (Controller) {
    return new Controller(logger);
  }
});