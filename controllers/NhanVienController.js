const _ = require('lodash');

const { convertInputToQueryDB } = require(`${process.cwd()}/utils/mssql.js`);
const packages = require('../mssql/package');
const code = require('../config/code');

class NhanVienController {
  constructor(logger) {
    this.logger = logger;
  }

  async getAll(req, res, next) {
    console.log("NhanVienController:getAll is called");
    try {
      let result = {
        code: code.CODE_SUCCESS,
        message: 'Success',
        data: {},
      }

      let danhSachNhanVien = await convertInputToQueryDB(packages.getListNhanVien, {});
      danhSachNhanVien = _.first(danhSachNhanVien);

      danhSachNhanVien = danhSachNhanVien.map(nhanVien => {
        return {
          MaNhanVien: nhanVien.MaNhanVien,
          TenNhanVien: nhanVien.TenNhanVien,
          mataikhoan: nhanVien.mataikhoan,
          tentaikhoan: nhanVien.tentaikhoan,
          TinhTrang: nhanVien.TinhTrang,
        }
      })

      result.data = {
        entries: danhSachNhanVien,
        meta: {
          total: danhSachNhanVien.length,
        }
      }

      return res.json(result);
    } catch (error) {
      console.error("NhanVienController::getAll::error: ", error);

      return next(error);
    }
  }
}

module.exports = NhanVienController;