const _ = require('lodash');

const { convertInputToQueryDB } = require(`${process.cwd()}/utils/mssql.js`);
const packages = require('../mssql/package');
const code = require('../config/code');
const helper = require('../utils/helper');

class NgayKhachDoanController {
  constructor(logger) {
    this.logger = logger;
  }

  async getAll(req, res, next) {
    console.log("NgayKhachDoanController:getAll is called");
    let { ngayNap, loaiKhach } = req.query
    try {
      let result = {
        code: code.CODE_SUCCESS,
        message: 'Success',
        data: {},
      }

      loaiKhach = +loaiKhach || 2;

      ngayNap = ngayNap || helper.getDateNowByFormat('YYYY-MM-DD');

      let inputs = {
        loaiKhach,
        ngayNap: `'${ngayNap}'`,
        maNhanVien: 63,
      }
      console.log("inputs: ", inputs);

      let doanhThuNgayKhach = await convertInputToQueryDB(packages.doanhThuNgayKhachDoan, inputs);
      doanhThuNgayKhach = _.first(doanhThuNgayKhach);

      result.data = {
        entries: doanhThuNgayKhach,
        meta: {
          total: doanhThuNgayKhach.length,
        }
      }

      return res.json(result);
    } catch (error) {
      console.error("NgayKhachDoanController::getAll::error: ", error);

      return next(error);
    }
  }
}

module.exports = NgayKhachDoanController;