const _ = require('lodash');

const { convertInputToQueryDB } = require(`${process.cwd()}/utils/mssql.js`);
const packages = require('../mssql/package');
const code = require('../config/code');
const helper = require('../utils/helper');

class baoCaoTuanThangController {
  constructor(logger) {
    this.logger = logger;
  }

  async getAll(req, res, next) {
    console.log("baoCaoTuanThangController:getAll is called");
    let { tuNgay, denNgay, maGroupLoaiVe } = req.query
    try {
      let result = {
        code: code.CODE_SUCCESS,
        message: 'Success',
        data: {},
      }

      tuNgay = tuNgay || helper.getDateNowByFormat('YYYY-MM-DD');

      denNgay = denNgay || helper.getDateNowByFormat('YYYY-MM-DD');

      maGroupLoaiVe = +maGroupLoaiVe || 1

      let inputs = {
        maGroupLoaiVe,
        tuNgay: `'${tuNgay}'`,
        denNgay: `'${denNgay}'`,
      }
      console.log("inputs: ", inputs);

      let baoCaoTuan = await convertInputToQueryDB(packages.doanhThuTuanThang, inputs);
      baoCaoTuan = baoCaoTuan[0];

      result.data = {
        entries: baoCaoTuan,
        meta: {
          total: baoCaoTuan.length,
        }
      }

      return res.json(result);
    } catch (error) {
      console.error("baoCaoTuanThangController::getAll::error: ", error);

      return next(error);
    }
  }
}

module.exports = baoCaoTuanThangController;