const jwt = require('jsonwebtoken');
const config = require('config');

const { convertInputToQueryDB } = require(`${process.cwd()}/utils/mssql.js`);
const packages = require('../mssql/package');
const code = require('../config/code');

module.exports = async (req, res, next) => {
  console.log("Middlewares::validateToken is called");
  try {
    let token = req.query.token || req.body.token || req.headers.token || req.cookies["st_token"] || req.cookies["sv_token"]

    if (!token) {
      return res.json({
        code: code.CODE_UNAUTHORIZED,
        message: "Any of the listed authentication credentials are missing",
        status: "error",
      })
    }

    let decoded = await jwt.verify(token, config['jwtSecretKey'])

    let inputs = {
      maNhanVien: +decoded.maNhanVien
    }
    console.log("inputs: ", inputs);

    let infoAccount = await convertInputToQueryDB(packages.getInfoAccount, inputs);
    infoAccount = infoAccount[0];

    if (infoAccount.length === 0) {
      return res.json({
        code: code.CODE_UNAUTHORIZED,
        message: 'User not found',
        data: {}
      })
    }

    req.query.maNhanVien = infoAccount[0].MaNhanVien;

    return next();
  } catch (error) {
    console.error("AccountController::login::error: ", error);
    if (error.name === 'TokenExpiredError') {
      return res.json({
        code: code.CODE_UNAUTHORIZED,
        message: "Token is expired",
        data: {}
      })
    }

    return next(error);
  }
}