const express = require('express');
const config = require('config');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const http = require('http');
const path = require('path');

const code = require(`${process.cwd()}/config/code.js`);
const validateToken = require('./middlewares/validateToken');

const app = express();

const server = http.Server(app);

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.all('*', function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, token');
  return next();
});

app.use('/hub', express.static(path.join(__dirname, 'public')));

app.use('/ping', (req, res) => { return res.json('pong') });

app.use('/api/login', require('./routes/account'));

app.use(validateToken);

app.use('/api/doanh-thu-ngay-khach', require('./routes/ngayKhachDoan'));

app.use('/api/doanh-thu-ngay-nhan-vien', require('./routes/ngayNhanVien'));

app.use('/api/bao-cao-tuan-thang', require('./routes/baoCaoTuanThang'));

app.use('/api/bao-cao-nam', require('./routes/baoCaoNam'));

app.use('/api/danh-sach-nhan-vien', require('./routes/nhanVien'));


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  return res.json({
    code: code.CODE_UNSUPPORTED_API,
    message: "unsupported api"
  });
});

app.use(function (err, req, res, next) {

  return res.json({
    code: code.CODE_ERROR,
    message: err
  });
  // // error handlers, development error handler will print stacktrace
  // if (app.get('env') === 'development' || app.get('env') === 'local' || app.get('env') === 'sandbox') {
  //   return res.json({
  //     code: code.CODE_ERROR,
  //     message: err.stack
  //   });
  // } else {
  //   // production error handler, no stacktrace leaked to user
  //   return res.json({
  //     code: code.CODE_ERROR,
  //     message: err.message
  //   });
  // }
});

port = config.app.port;
server.listen(port, function () {
  console.log(`Express server listening on port ${port}`)
});

module.exports = server;