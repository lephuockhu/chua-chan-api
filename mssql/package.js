module.exports = {
  doanhThuNgayKhachDoan: "PSP_Them_0101_ThongKe_LayDoanhThuNgayKhachDoan :STRING(ngayNap), :NUMBER(maNhanVien), :NUMBER(loaiKhach), :CURSOR",
  doanhThuNgayNhanVienBanVe: "PSP_Them_0102_ThongKe_DoanhThuNgay_NhanVienBanVe :STRING(ngayNap), :NUMBER(viTri), :NUMBER(maGroupLoaiVe), :NUMBER(maNhanVien), :CURSOR",
  doanhThuTuanThang: "PSP_Them_0104_ThongKe_DoanhThuTuan_Thang :NUMBER(maGroupLoaiVe), :STRING(tuNgay), :STRING(denNgay), :CURSOR",
  baoCaoDoanhThuNam: "PSP_LayBaoCaoDoanhThuNam :NUMBER(maGroupLoaiVe), :NUMBER(nam), :CURSOR",
  getListNhanVien: "PSP_NhanVien_SelectAll :CURSOR",
  loginAccount: "PSP_LayThongTinDangNhap :STRING(taiKhoan), :STRING(matKhau), :CURSOR",
  getInfoAccount: "PSP_NhanVien_Select_MaNhanVien :NUMBER(maNhanVien), :CURSOR",
}